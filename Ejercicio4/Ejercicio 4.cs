/*
A partir de las clases CajaRepository y SucursalRepository, crear la clase BaseRepository<T> 
que unifique los métodos GetAllAsync y GetOneAsync.
Crear un abstract BaseEntity que defina la property Id y luego modificar las entities Caja y Sucursal para que hereden de BaseEntity 
Aclaración: Se deben respetar la interfaces. 
*/

namespace Domain.Entities
{
	public abstract class BaseEntity
    {
        public object Id { get; set; }
    }
	
    public class Caja:BaseEntity
    {
        public int SucursalId { get; }
        public string Descripcion { get; }
        public int TipoCajaId { get; }

        public Caja(Guid id, int sucursalId, string descripcion, int tipoCajaId)
        {
            Id = id;
            SucursalId = sucursalId;
            Descripcion = descripcion;
            TipoCajaId = tipoCajaId;
        }
    }

    public class Sucursal:BaseEntity
    {
        public string Direccion { get; }
        public string Telefono { get; }

        public Sucursal(int id, string direccion, string telefono)
        {
            Id = id;
            Direccion = direccion;
            Telefono = telefono;
        }
    }
}

namespace Infrastructure.Data.Repositories
{
	public interface ICajaRepository
    {
        Task<IEnumerable<Caja>> GetAllAsync();
        Task<Caja> GetOneAsync(Guid id);
    }
	
	public interface ISucursalRepository
    {
        Task<IEnumerable<Sucursal>> GetAllAsync();
        Task<Sucursal> GetOneAsync(int id);
    }

    public interface IBaseRepository<T>
    {
        Task<IEnumerable<T>> GetAllAsync<T>();
        Task<T> GetOneAsync<T>(Guid id);
        Task<T> GetOneAsync<T>(int id);
    }

    public class CajaRepository
    {
        private readonly DataContext _db;

        public CajaRepository(DataContext db)
            => _db = db;

        public async Task<IEnumerable<Caja>> GetAllAsync()
            => await _db.Cajas.ToListAsync();

        public async Task<Caja> GetOneAsync(Guid id)
            => await _db.Cajas.FirstOrDefaultAsync(x => x.Id == id);
    }

    public class SucursalRepository
    {
        private readonly DataContext _db;

        public CajaRepository(DataContext db)
            => _db = db;

        public async Task<IEnumerable<Sucursal>> GetAllAsync()
            => await _db.Sucursales.ToListAsync();

        public async Task<Sucursal> GetOneAsync(int id)
            => await _db.Sucursales.FirstOrDefaultAsync(x => x.Id == id);
    }

    public class BaseRepository<T>
    {
        private readonly DataContext _db;

        public BaseRepository(DataContext db)
            => _db = db;

        public async Task<IEnumerable<T>> GetAllAsync<T>()
        {
            //se determina el tipo de T
            var table = typeof(T);
            //se obtiene el nombre de la tabla a la que hace referencia T
            var nameTable = table.Name;
            //si el nombre es Caja, realiza la consulta

            if (nameTable.Equals("Caja"))
                return await _db.Cajas.ToListAsync();
            //si el nombre es Sucursal, realiza la consulta
            if (nameTable.Equals("Sucursal"))
                return await _db.Sucursales.ToListAsync();

            //si el nombre no es Caja ni Sucursal, emite excepcion
            return throw new Exception("Tipo de entidad no soportada");
        }

        public async Task<T> GetOneAsync<T>(Guid id)
        {
            //se determina el tipo de T
            var table = typeof(T);
            //se obtiene el nombre de la tabla a la que hace referencia T
            var nameTable = table.Name;
            //si el nombre es Caja, realiza la consulta
            if (nameTable.Equals("Caja"))
                return await _db.Cajas.FirstOrDefaultAsync(x => x.Id == id);
            //si el nombre no es Caja, emite excepcion
            return throw new Exception("Tipo de entidad no soportada");
        }

        public async Task<T> GetOneAsync<T>(int id)
        {
            //se determina el tipo de T
            var table = typeof(T);
            //se obtiene el nombre de la tabla a la que hace referencia T
            var nameTable = table.Name;
            //si el nombre es Sucursal, realiza la consulta
            if (nameTable.Equals("Sucursal"))
                return await _db.Sucursales.FirstOrDefaultAsync(x => x.Id == id);
            //si el nombre no es Sucursal, emite excepcion
            return throw new Exception("Tipo de entidad no soportada");
        }
    }
}