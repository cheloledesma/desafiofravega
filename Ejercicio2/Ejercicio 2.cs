/*
Teniendo en cuenta la librería ICache, que fue escrita e implementada por otro equipo y utiliza una cache del tipo Key Value,
tomar la clase CajaRepository y modificar los métodos AddAsync, GetAllAsync, GetAllBySucursalAsync y GetOneAsync para que utilicen cache.

Datos:
    * Existen en la empresa 20 sucursales
    * Como mucho hay 100 cajas en la base

Restricción:    
	* Solo es posible utilizar 1 key (IMPORTANTE)
	
Aclaración:
	* No realizar una implementación de ICache, otro equipo la esta brindando
*/

public interface ICache
{
    Task AddAsync<T>(string key, T obj, int? durationInMinutes);
    Task<T> GetOrDefaultAsync<T>(string key);
    Task RemoveAsync(string key);
}

public class CajaRepository
{
    #region Attributes
    private readonly DataContext _db;
    private readonly ICache _cache;
    private List<Entities.Caja> _listCajas;
    #endregion

    #region Constructors
    public CajaRepository(DataContext db, ICache cache)
    {
        _db = db?? throw new ArgumentNullException(nameof(DataContext);
        _cache=cache;
        _listCajas = new List<Entities.Caja>();
    }
    #endregion

    #region Expose methods
    public async Task AddAsync(Entities.Caja caja)
    {
        await _db.Cajas.AddAsync(caja);
        await _db.SaveChangesAsync();

        //se obtiene de cache el objeto List<Entities.Caja> de la key "cajas" para adicionar el nuevo objeto caja que se insertó en la bd
        _listCajas = await _cache.GetOrDefaultAsync("cajas");
        //se controla si la lista tiene datos
        if(_listCajas == null)
        {
            //si no tiene datos, se inicializa la lista de cajas
            _listCajas = new List<Entities.Caja>(){caja};            
        }
        else
        {
            //se adiciona el nuevo objeto Entities.Caja a la lista obtenida de caché
            _listCajas.Add(caja);
        }
        //se elimina el objeto de caché con key "cajas"
        await _cache.RemoveAsync("cajas");
        //se cachea la mueva lista con el nuevo objeto en key "cajas"
        await _cache.AddAsync("cajas", _listCajas, 480);
    }

    public async Task<IEnumerable<Entities.Caja>> GetAllAsync()
    {
        //se obtiene de cache el objeto List<Entities.Caja> de la key "cajas"
        _listCajas = await _cache.GetOrDefaultAsync("cajas");
        return _listCajas;
    }

    public async Task<IEnumerable<Entities.Caja>> GetAllBySucursalAsync(int sucursalId)
    {
        //se obtiene de cache el objeto List<Entities.Caja> de la key "cajas"
        _listCajas = await _cache.GetOrDefaultAsync("cajas");
        //se aplica filtro y se retorna el resultado
        return _listCajas.Where(x => x.SucursalId == sucursalId).ToList();
    }

    public async Task<Entities.Caja> GetOneAsync(Guid id)
    {
        //se obtiene de cache el objeto List<Entities.Caja> de la key "cajas"
        _listCajas = await _cache.GetOrDefaultAsync("cajas");
        //se aplica filtro y se retorna el resultado
        return _listCajas.FirstOrDefault(x => x.Id == id);
    }
    #endregion
}